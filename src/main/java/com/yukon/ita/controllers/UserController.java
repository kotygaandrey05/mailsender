package com.yukon.ita.controllers;

import com.yukon.ita.user.User;
import com.yukon.ita.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @GetMapping(value = "/user/{id}")
    public Optional<User> getUserById(@PathVariable("id") Long id) {
        return userService.findById(id);
    }

    @GetMapping(value = "/periodicals")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.findAll();
        return ResponseEntity.ok().body(users);
    }

    @PostMapping(value = "/user/save")
    public User insert(@RequestBody User user) {
        return userService.insert(user);
    }

    @PutMapping(value = "/user/update/{id}")
    public ResponseEntity<User> update(@PathVariable("id") Long id, @RequestBody User user) {
        userService.update(user, id);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping(value = "/user/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
        userService.deleteById(id);
    }
}
