package com.yukon.ita.controllers;

import com.yukon.ita.template.Template;
import com.yukon.ita.template.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TemplateController {

    private final TemplateService templateService;

    @Autowired
    public TemplateController(TemplateService templateService){
        this.templateService = templateService;
    }

    @GetMapping(value = "/template/{id}")
    public Optional<Template> getTemplateById(@PathVariable("id") Long id) {
        return templateService.findById(id);
    }

    @GetMapping(value = "/templates")
    public ResponseEntity<List<Template>> getAllTemplates() {
        List<Template> templates = templateService.findAll();
        return ResponseEntity.ok().body(templates);
    }

    @PostMapping(value = "/template/save")
    public Template insert(@RequestBody Template template) {
        return templateService.insert(template);
    }

    @PutMapping(value = "/template/update/{id}")
    public ResponseEntity<Template> update(@PathVariable("id") Long id, @RequestBody Template template) {
        templateService.update(template, id);
        return ResponseEntity.ok(template);
    }

    @DeleteMapping(value = "/template/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
        templateService.deleteById(id);
    }
}