package com.yukon.ita.controllers;

import com.yukon.ita.mail.MailResponse;
import com.yukon.ita.mail.MailServices;
import com.yukon.ita.recipient.Recipient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/mail")
public class SendMailController {

    @Autowired
    private MailServices smtpMailSender;

    @PostMapping("/send")
    public MailResponse sendEmail(@RequestBody Recipient request){
        Map model = new HashMap();
        model.put("recepientSubject",request.getRecipientSubject());
        model.put("recepientText",request.getRecipientText());
        model.put("recepientEmail",request.getRecipientEmail());
        return smtpMailSender.sendSimpleMessage(request,model);
    }

   /* @PostMapping("/send/template")
    public MailResponse sendEmailWithTemplate(@RequestBody Recipient recipient){
        return smtpMailSender.sendEmailWithTemplate(recipient);
    }*/
}
