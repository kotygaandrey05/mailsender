package com.yukon.ita.recipient;

import com.yukon.ita.template.Template;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class RecipientDTO {

    private Long recipientId;
    private String recipientEmail;
    private String recipientSubject;
    private Object recipientText;
    private List<Template> templates;

    public RecipientDTO(Long recipientId, String recipientEmail, String recipientSubject, Object recipientText, List<Template> templates) {
        this.recipientId = recipientId;
        this.recipientEmail = recipientEmail;
        this.recipientSubject = recipientSubject;
        this.recipientText = recipientText;
        this.templates = templates;
    }

    public Long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Long recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientSubject() {
        return recipientSubject;
    }

    public void setRecipientSubject(String recipientSubject) {
        this.recipientSubject = recipientSubject;
    }

    public Object getRecipientText() {
        return recipientText;
    }

    public void setRecipientText(Object recipientText) {
        this.recipientText = recipientText;
    }

    public List<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(List<Template> templates) {
        this.templates = templates;
    }
}
